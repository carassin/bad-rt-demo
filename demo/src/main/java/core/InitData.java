package core;

public abstract class InitData {

    public static final String
        DEVICE = "fd31ddd1",
        PLATFORM = "Android",
        UNLOCK_APP = "C:\\Appium\\node_modules\\appium\\build\\unlock_apk\\unlock_apk-debug.apk",
        PACKAGE = "com.revolut.revolut.test",
        ACTIVITY = "com.revolut.ui.login.pin.LoginActivity",
        APPIUM_IP = "127.0.0.1",
        APPIUM_PORT = "9515";
}
