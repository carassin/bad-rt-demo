package core;

import java.util.Random;

public abstract class TestData {

    public static final String
        LOGIN = "662266",
        TEST_COUNTRY = "Австрия",
        TEST_CURRENCY = "Euro",
        TEST_CURRENCY_ACRONYM = "EUR",
        TEST_IBAN_CORRECT = "AT611904300234573201",
        TEST_IBAN_INCORRECT = "00611904300234573201",
        TEST_SWIFT_CORRECT = "CTBAAT2S",
        TEST_SWIFT_INCORRECT = "CTBAKK2S",
        TEST_PHONE = "+79001002030",
        TEST_MAIL = "testEmail@gmail.com";

    public static final int PASSWORD = 1111;

    public static String generateRandomString(int length){
        String Dictionary = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        StringBuilder sb = new StringBuilder();
        Random rnd = new Random();
        while (sb.length() < length) {
            int index = (int) (rnd.nextFloat() * Dictionary.length());
            sb.append(Dictionary.charAt(index));
        }
        String result = sb.toString();
        return result;
    }
}
