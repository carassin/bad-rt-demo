package screens;

import core.InitData;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;


import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public abstract class Abstract {

    public static AndroidDriver driver;

    public Abstract(){
        this(initDriver());
    }

    public Abstract(AndroidDriver driver){
        this.driver = driver;
    }
    public static final String idPrefix = "com.revolut.revolut.test:id";

    public static AndroidDriver initDriver() {
        AndroidDriver driver = null;
        DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
        desiredCapabilities.setCapability("deviceName", InitData.DEVICE);
        desiredCapabilities.setCapability("platformName", InitData.PLATFORM);
        desiredCapabilities.setCapability("app", InitData.UNLOCK_APP);
        desiredCapabilities.setCapability("appPackage", InitData.PACKAGE);
        desiredCapabilities.setCapability("appActivity", InitData.ACTIVITY);
        desiredCapabilities.setCapability("--session-override", true);
        try {
            driver = new AndroidDriver(new URL("http://"+InitData.APPIUM_IP+":"+InitData.APPIUM_PORT+"/wd/hub"), desiredCapabilities);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return driver;
    }

    protected void findByIdAndClick(String id){
        driver.findElement(By.id(id)).click();
    }


    protected String findByIdAndGetText(String id){
       return driver.findElement(By.id(id)).getText();
    }

    protected List<String> findAllByIdAndGetText(String id){
        List<String> result = new ArrayList<String>();
        List<WebElement> elements = driver.findElements(By.id(id));
        for (WebElement element:elements){
            result.add(element.getText());
        }
        return result;
    }

    protected void findByTextAndClick(String text){
        driver.findElement(By.xpath("//*[@text='" +text + "']")).click();
    }

    protected void findByIdAndSendKeys(String id, String line, boolean clearFields){
        if(clearFields) {
            driver.findElement(By.id(id)).clear();
        }
        driver.findElement(By.id(id)).sendKeys(line);
    }
    protected void findByIdAndSendKeys(String id, String line){
        findByIdAndSendKeys(id, line, true);
    }


    protected boolean isElementDisplayedById(String id){
        return driver.findElement(By.id(id)).isDisplayed();
    }

    protected boolean isElementEnabledById(String id){
        return driver.findElement(By.id(id)).isEnabled();
    }

    protected boolean isElementSelectedById(String id){
        return driver.findElement(By.id(id)).isSelected();
    }

    protected void clearFieldById(String id){
        driver.findElement(By.id(id)).clear();
    }

    protected boolean isElementPresentById(String id){
        return driver.findElements(By.id(id)).size()>0;
    }


    protected boolean isDriverRunning() {
        return !(driver == null || driver.toString().contains("null"));
    }

    public void close() {
        if (isDriverRunning()) {
            driver.quit();
            System.out.println(driver + "driver closed");
        } else
            System.out.println(driver + "driver already has been closed");
    }




}
