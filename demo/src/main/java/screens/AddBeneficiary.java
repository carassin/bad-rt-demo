package screens;

import io.appium.java_client.android.AndroidDriver;

public class AddBeneficiary extends Basic {

    public AddBeneficiary() {
        this(initDriver());
    }

    public AddBeneficiary(AndroidDriver driver) {
        super(driver);
    }



    private final String
            successMessageId = idPrefix+"/operation_state_title",
            doneButtonId = idPrefix+"/operation_status_button",
            defaultName = "defaultName",
            successMessageText = "Beneficiary "+defaultName+" was successfully created";


    public String getNameFromSuccessMessage(){
        waitForIdToLoad(doneButtonId, 5);
        return findByIdAndGetText(successMessageId)
                .replace(successMessageText.split(defaultName)[0], "")
                .replace(successMessageText.split(defaultName)[1], "");
    }
    public void clickDoneButton(){
        findByIdAndClick(doneButtonId);
    }
}
