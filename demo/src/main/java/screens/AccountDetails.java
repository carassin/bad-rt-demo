package screens;

import core.TestData;
import io.appium.java_client.android.AndroidDriver;

public class AccountDetails extends Basic {

    public AccountDetails() {
        this(initDriver());
    }

    public AccountDetails(AndroidDriver driver) {
        super(driver);
    }



    private final String
            firstNameFieldId = idPrefix+"/first_name",
            lastNameFieldId = idPrefix+"/last_name",
            accountNumberFieldId = idPrefix+"/server_field_0",
            bicSwiftFieldId = idPrefix+"/server_field_1",
            mobilePhoneFieldId = idPrefix+"/mobile_phone",
            emailAddressFieldId = idPrefix+"/email",
            companyNameFieldId = idPrefix+"/organization_name",
            countryFieldId = idPrefix+"/wrapper_country",
            postalCodeFieldId = idPrefix+"/address_postal_code",
            address1FieldId = idPrefix+"/address_line_1",
            address2FieldId = idPrefix+"/address_line_2",
            cityFieldId = idPrefix+"/address_city",
            regionFieldId = idPrefix+"/address_region",
            alertMessageId = "android:id/message",
            alertOKButtonId = "android:id/button1";
    public static final String
            invalidBicMessage = "Invalid BIC country code",
            invalidIbanMessage = "Invalid IBAN: "+ TestData.TEST_IBAN_INCORRECT;

    public void fillInAForm1
            (boolean isToBusiness, String firstName, String lastName, String companyName, String accNumber,
             String bicSwift, String phone, String email, boolean clearFields){
        if(isToBusiness) {
            findByIdAndSendKeys(companyNameFieldId, companyName, clearFields);
        }else {
            findByIdAndSendKeys(firstNameFieldId, firstName, clearFields);
            findByIdAndSendKeys(lastNameFieldId, lastName, clearFields);
        }
        findByIdAndSendKeys(accountNumberFieldId, accNumber, clearFields);
        findByIdAndSendKeys(bicSwiftFieldId, bicSwift, clearFields);
        findByIdAndSendKeys(mobilePhoneFieldId, phone, clearFields);
        findByIdAndSendKeys(emailAddressFieldId, email, clearFields);
    }
    public void fillInAForm1
            (boolean isToBusiness, String firstName, String lastName, String companyName, String accNumber,
             String bicSwift, String phone, String email){
        fillInAForm1(isToBusiness, firstName, lastName, companyName, accNumber,bicSwift,phone,email,true);
    }

    public void fillInAForm2(String country, String postal, String address1, String address2, String city, String region, boolean clearFields){
        findByIdAndClick(countryFieldId);
        findByTextAndClick(country);
        findByIdAndSendKeys(postalCodeFieldId, postal, clearFields);
        findByIdAndSendKeys(address1FieldId, address1, clearFields);
        findByIdAndSendKeys(address2FieldId, address2, clearFields);
        findByIdAndSendKeys(cityFieldId, city, clearFields);
        findByIdAndSendKeys(regionFieldId, region, clearFields);
    }
    public void fillInAForm2(String country, String postal, String address1, String address2, String city, String region){
        fillInAForm2(country, postal, address1, address2, city, region,true);
    }

    public String getAlertMessage(){
        return findByIdAndGetText(alertMessageId);
    }

    public void clickOKAtAlertBox(){
        waitForIdToLoad(alertOKButtonId, 3);
        findByIdAndClick(alertOKButtonId);
    }

    public void waitForAlert(){
        waitForIdToLoad(alertOKButtonId, 5);
    }

    public void clearFirstName(){
        clearFieldById(firstNameFieldId);
    }
    public void clearLastName(){
        clearFieldById(lastNameFieldId);
    }
    public void clearCompanyName(){
        clearFieldById(companyNameFieldId);
    }
    public void clearIBANNumber(){
        clearFieldById(accountNumberFieldId);
    }
    public void clearSwiftNumber(){
        clearFieldById(bicSwiftFieldId);
    }
    public void clearMobileNumber(){
        clearFieldById(mobilePhoneFieldId);
    }
    public void clearEmail(){
        clearFieldById(emailAddressFieldId);
    }
    public void typeFirstName(String s){
        findByIdAndSendKeys(firstNameFieldId, s);
    }
    public void typeLastName(String s){
        findByIdAndSendKeys(lastNameFieldId, s);
    }
    public void typeCompanyName(String s){
        findByIdAndSendKeys(companyNameFieldId, s);
    }
    public void typeIBANNumber(String s){
        findByIdAndSendKeys(accountNumberFieldId, s);
    }
    public void typeSwiftNumber(String s){
        findByIdAndSendKeys(bicSwiftFieldId, s);
    }
    public void typeMobileNumber(String s){
        findByIdAndSendKeys(mobilePhoneFieldId, s);
    }
    public void typeEmail(String s){
        findByIdAndSendKeys(emailAddressFieldId, s);
    }



}
