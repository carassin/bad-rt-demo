package screens;

import io.appium.java_client.android.AndroidDriver;

import java.util.List;

public class BankTransfer extends Basic {

    public BankTransfer() {
        this(initDriver());
    }

    public BankTransfer(AndroidDriver driver) {
            super(driver);
    }



    private final String
            addNewBeneficiaryText = "Add a new beneficiary",
            toMyselfText = "To myself",
            toAnotherPersonText = "To another person",
            toABusinessText = "To a business",

            chooseCountryId = idPrefix+"/edit_country",
            chooseCurrencyId = idPrefix+"/edit_currency",
            nextButtonId = idPrefix+"/button_next",

            beneficiaryTitleId = idPrefix+"/item_title";

    public enum TransferType {TO_MYSELF, TO_ANOTHER_PERSON, TO_A_BUSINESS}

    public void chooseTransferType(TransferType transferType){
        findByTextAndClick(addNewBeneficiaryText);
        switch(transferType){
            case TO_MYSELF:
                findByTextAndClick(toMyselfText);
                break;
            case TO_ANOTHER_PERSON:
                findByTextAndClick(toAnotherPersonText);
                break;
            case TO_A_BUSINESS:
                findByTextAndClick(toABusinessText);
                break;
        }findByIdAndClick(nextButtonId);
    }

    public void chooseCountryAndCurrency(String country, String currency){
        findByIdAndClick(chooseCountryId);
        findByTextAndClick(country);
        findByIdAndClick(chooseCurrencyId);
        findByTextAndClick(currency);
        findByIdAndClick(nextButtonId);
    }


    public boolean isBeneficiaryAddedByNameAndCurrency(String name, String currency){
        for (String s:getAllVisibleBeneficiaries()){
            if(s.equals(name+" ∙ "+currency)){
                return true;
            }
        }
        return false;
    }

    private List<String> getAllVisibleBeneficiaries(){
        return findAllByIdAndGetText(beneficiaryTitleId);
    }


}
