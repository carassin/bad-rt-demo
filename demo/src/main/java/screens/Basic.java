package screens;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Basic extends Abstract{

    public Basic(AndroidDriver driver) {
        super(driver);
    }


    public Basic(){
        this(initDriver());
    }
    private final String
            skipButtonId = idPrefix+"/header_next",
            nextButtonId = idPrefix+"/button_next",
            backButtonId = idPrefix+"/back_button",
            searchButtonId = idPrefix+"/search_button",
            searchFieldId = idPrefix+"/search_src_text";

    protected void waitForIdToLoad(String id, int seconds){
        WebDriverWait wait = new WebDriverWait(driver, seconds);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(id)));
    }

    public void skip(){
        waitForIdToLoad(skipButtonId, 3);
        System.out.println("DDD");
        if(isElementPresentById(skipButtonId)){
            findByIdAndClick(skipButtonId);
        }
    }
    public void next(){
        findByIdAndClick(nextButtonId);
    }

    public void back(){
        findByIdAndClick(backButtonId);
    }
    public boolean isBackButtonPresent(){
        return(isElementPresentById(backButtonId));
    }
    public boolean isNextButtonEnabled(){
        return isElementEnabledById(nextButtonId);
    }
}
