package screens;

import io.appium.java_client.android.AndroidDriver;

public class HomePage extends Basic {

    public HomePage() {
        this(initDriver());
    }

    public HomePage(AndroidDriver driver) {
        super(driver);
    }



    public enum TransferOption{SEND_MONEY, REQUEST_MONEY, SPLIT_BILL, TO_BANK_ACCOUNT}

    private final String
            transferButtonId = idPrefix+"/button_transfer",
            sendMoneyOptionText = "Send money",
            requestMoneyOptionText = "Request money",
            splitBillOptionText = "Split bill",
            toBankAccountOptionText = "To bank account";

    public void openTransferMenu(TransferOption transferOption){
        findByIdAndClick(transferButtonId);
        switch(transferOption){
            case SEND_MONEY:
                findByTextAndClick(sendMoneyOptionText);
                break;
            case REQUEST_MONEY:
                findByTextAndClick(requestMoneyOptionText);
                break;
            case SPLIT_BILL:
                findByTextAndClick(splitBillOptionText);
                break;
            case TO_BANK_ACCOUNT:
                findByTextAndClick(toBankAccountOptionText);
                break;
        }skip();
    }
}
