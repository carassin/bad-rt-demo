package screens;

import io.appium.java_client.android.AndroidDriver;
import org.testng.Assert;

public class MobileLogin extends Basic {


    public MobileLogin() {
        this(initDriver());
    }

    public MobileLogin(AndroidDriver driver) {
        super(driver);
    }




    private final String
            phoneNumberField = idPrefix+"/uic_edit_phone_number",
            signupNextextButtonId = idPrefix+"/signup_next",
            passDigitId = idPrefix+"/digit",
            notNowButtonId = idPrefix+"/uic_header_next";


    private void enterPassword(int pwd){
        Assert.assertTrue(pwd>999&&pwd<10000, "password should contain 4 digits.");
        for(int i=0;i<String.valueOf(pwd).toCharArray().length;i++){
            findByIdAndClick(passDigitId+String.valueOf(pwd).toCharArray()[i]);
        }
    }

    private void enterPhone(String phone){
        findByIdAndSendKeys(phoneNumberField, phone);
    }

    public void login(String phone, int pwd){
        skip();
        enterPhone(phone);
        findByIdAndClick(signupNextextButtonId);
        enterPassword(pwd);
        waitForIdToLoad(notNowButtonId, 5);
        if(isElementPresentById(notNowButtonId)) {
            findByIdAndClick(notNowButtonId);
        }
    }
}
