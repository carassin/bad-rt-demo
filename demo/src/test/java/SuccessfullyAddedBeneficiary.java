import core.TestData;
import org.testng.annotations.*;
import org.testng.asserts.SoftAssert;
import screens.*;

/*
* This positive test checks if a beneficiary successfully added if filling in account details fields correctly.
* Executed for every transfer type via DataProvider.
* */

public class SuccessfullyAddedBeneficiary extends Basic{


    MobileLogin mobileLogin;
    HomePage homePage;

    @DataProvider(name = "testData")
    public Object[][] testData(){
        return new Object[][]{
                {BankTransfer.TransferType.TO_MYSELF, false},
                {BankTransfer.TransferType.TO_ANOTHER_PERSON, false},
                {BankTransfer.TransferType.TO_A_BUSINESS, true}

        };
    }

    @BeforeTest
    public void beforeTest(){

        mobileLogin = new MobileLogin(driver);
        mobileLogin.login(TestData.LOGIN, TestData.PASSWORD);
        homePage = new HomePage(driver);
        homePage.openTransferMenu(HomePage.TransferOption.TO_BANK_ACCOUNT);
    }


    @AfterTest
    public void afterTest(){
        homePage.close();
    }

    @Test(dataProvider = "testData")
    public void test(BankTransfer.TransferType transferType, boolean isToBusiness){
        String firstName = TestData.generateRandomString(5);
        String lastName = TestData.generateRandomString(10);
        String companyName = TestData.generateRandomString(8);
        BankTransfer bankTransfer = new BankTransfer(driver);
        bankTransfer.chooseTransferType(transferType);
        bankTransfer.chooseCountryAndCurrency(TestData.TEST_COUNTRY, TestData.TEST_CURRENCY);
        AccountDetails accountDetails = new AccountDetails(driver);
        accountDetails.fillInAForm1(isToBusiness, firstName, lastName, companyName,
                TestData.TEST_IBAN_CORRECT, TestData.TEST_SWIFT_CORRECT, TestData.TEST_PHONE, TestData.TEST_MAIL);
        accountDetails.next();
        SoftAssert softAssert = new SoftAssert();
        AddBeneficiary beneficiary = new AddBeneficiary(driver);
        String beneficiaryOwnerName;
        if(isToBusiness){
            beneficiaryOwnerName = companyName;
        }else{
            beneficiaryOwnerName = firstName+" "+lastName;
        }
        softAssert.assertEquals(beneficiary.getNameFromSuccessMessage(), beneficiaryOwnerName);
        beneficiary.clickDoneButton();
        bankTransfer = new BankTransfer(driver);

        softAssert.assertTrue(bankTransfer.isBeneficiaryAddedByNameAndCurrency(beneficiaryOwnerName, TestData.TEST_CURRENCY_ACRONYM),
                "ERROR: beneficiary is not displayed in the beneficiaries list");
    }

}
