import core.TestData;
import org.testng.annotations.*;
import org.testng.asserts.SoftAssert;
import screens.*;

/*
 * This test checks if Next button disabled when a required field is not filled and is the button enabled when all
 * required fields filled and one of optional fields isn't. Via DataProvider, it launches test for every transfer type
 */

public class NextButtonValidation extends Basic{

    MobileLogin mobileLogin;
    HomePage homePage;

    @DataProvider(name = "testData")
    public Object[][] testData(){
        return new Object[][]{
                {BankTransfer.TransferType.TO_MYSELF, false},
                {BankTransfer.TransferType.TO_ANOTHER_PERSON, false},
                {BankTransfer.TransferType.TO_A_BUSINESS, true}

        };
    }

    @BeforeTest
    public void beforeTest(){
        mobileLogin = new MobileLogin(driver);
        mobileLogin.login(TestData.LOGIN, TestData.PASSWORD);
        homePage = new HomePage(driver);
        homePage.openTransferMenu(HomePage.TransferOption.TO_BANK_ACCOUNT);
    }


    @AfterTest
    public void afterTest(){
        homePage.close();
    }

    @AfterMethod()
    public void afterMethod(){
        while(homePage.isBackButtonPresent()){
            homePage.back();
        }
    }

    @Test(dataProvider = "testData")
    public void test(BankTransfer.TransferType transferType, boolean isToBusiness) {
        String firstName = TestData.generateRandomString(5);
        String lastName = TestData.generateRandomString(10);
        String companyName = TestData.generateRandomString(8);
        BankTransfer bankTransfer = new BankTransfer(driver);
        bankTransfer.chooseTransferType(transferType);
        bankTransfer.chooseCountryAndCurrency(TestData.TEST_COUNTRY, TestData.TEST_CURRENCY);
        AccountDetails accountDetails = new AccountDetails(driver);
        accountDetails.fillInAForm1(isToBusiness, firstName, lastName, companyName,
                TestData.TEST_IBAN_CORRECT, TestData.TEST_SWIFT_CORRECT, TestData.TEST_PHONE, TestData.TEST_MAIL);

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertTrue(accountDetails.isNextButtonEnabled(), "ERROR: next button should be enabled when all form is filled");

        accountDetails.clearEmail();
        softAssert.assertTrue(accountDetails.isNextButtonEnabled(), "ERROR: next button should be enabled when optional field is empty");
        accountDetails.typeEmail(TestData.TEST_MAIL);

        accountDetails.clearMobileNumber();
        softAssert.assertTrue(accountDetails.isNextButtonEnabled(), "ERROR: next button should be enabled when optional field is empty");
        accountDetails.typeMobileNumber(TestData.TEST_PHONE);

        accountDetails.clearSwiftNumber();
        softAssert.assertTrue(!accountDetails.isNextButtonEnabled(), "ERROR: next button should be disabled when required field is empty");
        accountDetails.typeSwiftNumber(TestData.TEST_SWIFT_CORRECT);

        accountDetails.clearIBANNumber();
        softAssert.assertTrue(!accountDetails.isNextButtonEnabled(), "ERROR: next button should be disabled when required field is empty");
        accountDetails.typeIBANNumber(TestData.TEST_IBAN_CORRECT);

        //workaround since scrollTo doesn't work for some reason
        accountDetails.back();
        accountDetails.next();

        if (!isToBusiness) {
            accountDetails.clearFirstName();
            softAssert.assertTrue(!accountDetails.isNextButtonEnabled(), "ERROR: next button should be disabled when required field is empty");
            accountDetails.typeFirstName(firstName);

            accountDetails.clearLastName();
            softAssert.assertTrue(!accountDetails.isNextButtonEnabled(), "ERROR: next button should be disabled when required field is empty");
            accountDetails.typeLastName(lastName);
        } else {
            accountDetails.clearCompanyName();
            softAssert.assertTrue(!accountDetails.isNextButtonEnabled(), "ERROR: next button should be disabled when required field is empty");
            accountDetails.typeCompanyName(companyName);

        }

    }

}
