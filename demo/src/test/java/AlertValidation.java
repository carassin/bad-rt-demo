import core.TestData;
import org.testng.annotations.*;
import org.testng.asserts.SoftAssert;
import screens.*;

/*
* This test validates alert error message when IBAN or Swift numbers is filled incorrectly.
* Executed for every transfer type.
* */


public class AlertValidation extends Basic{

    MobileLogin mobileLogin;
    HomePage homePage;


    @DataProvider(name = "testData")
    public Object[][] testData(){
        return new Object[][]{
                {BankTransfer.TransferType.TO_MYSELF, false, TestData.TEST_IBAN_CORRECT, TestData.TEST_SWIFT_INCORRECT, AccountDetails.invalidBicMessage},
                {BankTransfer.TransferType.TO_MYSELF, false, TestData.TEST_IBAN_INCORRECT, TestData.TEST_SWIFT_CORRECT, AccountDetails.invalidIbanMessage},
                {BankTransfer.TransferType.TO_ANOTHER_PERSON, false, TestData.TEST_IBAN_CORRECT, TestData.TEST_SWIFT_INCORRECT, AccountDetails.invalidBicMessage},
                {BankTransfer.TransferType.TO_ANOTHER_PERSON, false, TestData.TEST_IBAN_INCORRECT, TestData.TEST_SWIFT_CORRECT, AccountDetails.invalidIbanMessage},
                {BankTransfer.TransferType.TO_A_BUSINESS, true, TestData.TEST_IBAN_CORRECT, TestData.TEST_SWIFT_INCORRECT, AccountDetails.invalidBicMessage},
                {BankTransfer.TransferType.TO_A_BUSINESS, true, TestData.TEST_IBAN_INCORRECT, TestData.TEST_SWIFT_CORRECT, AccountDetails.invalidIbanMessage}

        };
    }

    @BeforeTest
    public void beforeTest(){
        mobileLogin = new MobileLogin(driver);
        mobileLogin.login(TestData.LOGIN, TestData.PASSWORD);
        homePage = new HomePage(driver);
        homePage.openTransferMenu(HomePage.TransferOption.TO_BANK_ACCOUNT);
    }

    @AfterMethod()
    public void afterMethod(){
        while(homePage.isBackButtonPresent()){
            homePage.back();
        }
    }


    @AfterTest
    public void afterTest(){
        homePage.close();
    }

    @Test(dataProvider = "testData")
    public void test(BankTransfer.TransferType transferType, boolean isToBusiness, String iban, String swift, String expectedMessage){
        String firstName = TestData.generateRandomString(5);
        String lastName = TestData.generateRandomString(10);
        String companyName = TestData.generateRandomString(8);
        BankTransfer bankTransfer = new BankTransfer(driver);
        bankTransfer.chooseTransferType(transferType);
        bankTransfer.chooseCountryAndCurrency(TestData.TEST_COUNTRY, TestData.TEST_CURRENCY);
        AccountDetails accountDetails = new AccountDetails(driver);
        accountDetails.fillInAForm1(isToBusiness, firstName, lastName, companyName, iban, swift, TestData.TEST_PHONE, TestData.TEST_MAIL);
        accountDetails.next();
        SoftAssert softAssert = new SoftAssert();
        accountDetails.waitForAlert();
        softAssert.assertEquals(accountDetails.getAlertMessage(), expectedMessage,
                "ERROR: expected \""+expectedMessage+"\" but found \""+accountDetails.getAlertMessage()+"\"");
        accountDetails.clickOKAtAlertBox();
    }
}
